#ifndef PROCESS_MEMORY_H
# define PROCESS_MEMORY_H

# include <linux/init.h>
# include <linux/kernel.h>
# include <linux/linkage.h>
# include <linux/sched.h>
# include <linux/path.h>
# include <linux/sched/signal.h>
# include <linux/syscalls.h>
# include <linux/uaccess.h>
# include <linux/kallsyms.h>
# include <linux/fs_struct.h>
# include <linux/limits.h>
# include <linux/time.h>
# include <linux/pid.h>

# define _PM_CHILD_MAX 15006
# define _PM_NULL -2

struct pid_info {
	char	root[PATH_MAX];
	char	cwd[PATH_MAX];
	int	child[_PM_CHILD_MAX];
	const unsigned long *sp;
	unsigned long	etime;
	pid_t	pid;
	pid_t	ppid;
	int	state;
};

#endif
