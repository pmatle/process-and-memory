// SPDX-License-Identifier: GPL-2.0
# include "process_memory.h"

static int get_path(struct task_struct *task, struct pid_info *info, char *type)
{
	struct path	s_path;
	char		*path;
	char		*buff;
	int		err;

	if (strcmp(type, "root") == 0)
		get_fs_root(task->fs, &s_path);
	else
		get_fs_pwd(task->fs, &s_path);
	buff = kzalloc(sizeof(*buff) * PATH_MAX, GFP_USER);
	if (!buff)
		return -1;
	path = d_path(&s_path, buff, PATH_MAX);
	if (path == ERR_PTR(-ENAMETOOLONG)) {
		kfree(buff);
		return -ENAMETOOLONG;
	}
	if (strcmp(type, "root") == 0)
		err = copy_to_user(info->root, path, strlen(path));
	else
		err = copy_to_user(info->cwd, path, strlen(path));
	if (err)
		return -EFAULT;
	kfree(buff);
	return 0;
}

static int get_children(struct task_struct *task, struct pid_info *info)
{
	struct task_struct	*child;
	struct list_head	*list;
	int			err;
	int			x;

	x = 0;
	list_for_each(list, &task->children) {
		child = list_entry(list, struct task_struct, sibling);
		err = put_user(child->pid, &info->child[x]);
		if (err)
			return -EFAULT;
		x++;
	}
	info->child[x] = _PM_NULL;
	return 0;
}

static int get_elapsed_time(struct task_struct *task, struct pid_info *info)
{
	struct timespec	uptime;
	unsigned long	time;
	int		err;

	get_monotonic_boottime(&uptime);
	time = uptime.tv_sec - (task->start_time / 1000000000ULL);
	err = put_user(time, &info->etime);
	if (err)
		return -EFAULT;
	return 0;
}

static int get_directory_paths(struct task_struct *task, struct pid_info *info)
{
	int	err;

	err = get_path(task, info, "root");
	if (err)
		return err;
	err = get_path(task, info, "cwd");
	if (err)
		return err;
	return 0;
}

static int get_process_sp(struct task_struct *task, struct pid_info *info)
{
	unsigned long	*sp;
	struct pt_regs	*regs;
	int		err;

	regs = task_pt_regs(task);
	sp = get_stack_pointer(task, regs);
	err = put_user(sp, &(*(unsigned long **)&info->sp));
	if (err)
		return -EFAULT;
	return 0;
}

static int get_state(struct task_struct *task, struct pid_info *info)
{
	int	state;
	int	err;

	state = (task->state > 0) ? 1 : task->state;
	err = put_user(state, &info->state);
	if (err)
		return -EFAULT;
	return (0);
}

static int get_infor(struct task_struct *task, struct pid_info *pid_info)
{
	int			err;

	err = get_process_sp(task, pid_info);
	if (err)
		return err;
	err = get_directory_paths(task, pid_info);
	if (err)
		return err;
	err = put_user(task->pid, &pid_info->pid);
	if (err)
		return -EFAULT;
	err = put_user(task->parent->pid, &pid_info->ppid);
	if (err)
		return -EFAULT;
	err = get_state(task, pid_info);
	if (err)
		return err;
	err = get_children(task, pid_info);
	if (err)
		return err;
	err = get_elapsed_time(task, pid_info);
	if (err)
		return err;
	return 0;
}

SYSCALL_DEFINE2(get_pid_info, struct pid_info *, pid_info, int, pid)
{
	struct task_struct	*task;
	int			err;

	if (pid < 0 || !pid_info)
		return -EINVAL;
	for_each_process(task) {
		if (task->pid == pid) {
			err = get_infor(task, pid_info);
			if (err)
				return err;
			return 0;
		}
	}
	return -ESRCH;
}
