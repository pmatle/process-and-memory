DestDirName = process/
SRCS = srcs/Makefile srcs/pid_info.c srcs/process_memory.h
PATCHES= patches/*
HEADER = includes/process_memory.h
KDIR = /lib/modules/`uname -r`/build
MANPAGE = man_pages/get_pid_info.2

all:
	@/bin/mkdir $(KDIR)/process
	@/bin/cp -r $(SRCS) $(KDIR)/process/
	@/bin/mkdir $(KDIR)/mypatches
	@/bin/cp -r $(PATCHES) $(KDIR)/mypatches/
	cd $(KDIR)/include/linux/ && \
		/usr/bin/patch < $(KDIR)/mypatches/syscalls.h-patch && cd -
	cd $(KDIR)/include/uapi/asm-generic/ && \
		/usr/bin/patch < $(KDIR)/mypatches/unistd.h-patch && cd -
	cd $(KDIR)/kernel/ && patch < $(KDIR)/mypatches/sys_ni.c-patch && cd -
	cd $(KDIR)/arch/x86/entry/syscalls/ && \
		/usr/bin/patch < $(KDIR)/mypatches/syscall_64.tbl-patch && cd -
	cd $(KDIR)/ && /usr/bin/patch < $(KDIR)/mypatches/makefile.patch && \
		cd -
	/usr/bin/make bzImage -C $(KDIR)
	/usr/bin/make modules_install -C $(KDIR)
	/bin/cp -v $(KDIR)/arch/`uname -m`/boot/bzImage /boot/vmlinuz-marking
	/bin/cp -v $(KDIR)/System.map /boot/System.map-marking
	/bin/cp -v $(HEADER) /usr/include/
	/bin/cp -v $(MANPAGE) /usr/share/man/man2/
	/usr/bin/mandb
	/sbin/shutdown -r now

clean:
	/bin/rm -rf $(KDIR)/process
	/bin/rm -rf /usr/include/process_memory.h
	/bin/rm -rf /usr/share/man/man2/get_pid_info.2
	/bin/cp -v $(KDIR)/old_files/Makefile $(KDIR)/
	/bin/cp -v $(KDIR)/old_files/sys_ni.c $(KDIR)/kernel/
	/bin/cp -v $(KDIR)/old_files/syscall_64.tbl $(KDIR)/arch/x86/entry/syscalls/
	/bin/cp -v $(KDIR)/old_files/syscalls.h $(KDIR)/include/linux/
	/bin/cp -v $(KDIR)/old_files/unistd.h $(KDIR)/include/uapi/asm-generic/
	/bin/rm -rf $(KDIR)/mypatches
