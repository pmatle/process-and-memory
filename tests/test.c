#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <process_memory.h>

void	print_info(struct pid_info info)
{
	int	x;

	x = 0;
	printf("PID : %ld\n", info.pid);
	printf("STATE : %ld\n", info.state);
	printf("PPID : %ld\n", info.ppid);
	printf("ROOT : %s\n", info.root);
	printf("CWD : %s\n", info.cwd);
	printf("SP : %ld\n", info.sp);
	printf("ETIME : %ld\n", info.etime);
}

void	iterate_parents(struct pid_info child)
{
	struct pid_info	parent;
	int		pid;

	pid = child.ppid;
	printf("Parent processes : \n\n");
	while (get_pid_info(&parent, pid) != -1) {
		print_info(parent);
		pid = parent.ppid;
		printf("\n");
	}
}

void	iterate_children(struct pid_info parent)
{
	struct pid_info child;
	int		x;

	x = 0;
	printf("Children processes : \n\n");
	while (get_pid_info(&child, parent.child[x]) == 0) {
		print_info(child);
		printf("\n");
		x++;
		if (parent.child[x] == -2)
			break ;
	}
}

int	main(void)
{
	struct pid_info info;
	int		ret;

	ret = get_pid_info(&info, 168);
	if (ret == -1)
		perror("Error ");
	else {
		print_info(info);
		printf("\n");
		iterate_parents(info);
		printf("\n");
		iterate_children(info);
	}
	return (0);
}
