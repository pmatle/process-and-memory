#ifndef PROCESS_MEMORY_H
# define PROCESS_MEMORY_H

# include <sys/syscall.h>
# include <sys/types.h>

# define __NR_get_pid_info 335

struct pid_info {
	char	root[4096];
	char	cwd[4096];
	int	child[15006];
	const unsigned long *sp;
	unsigned long	etime;
	pid_t	pid;
	pid_t	ppid;
	int	state;
};

int	get_pid_info(struct pid_info *ret, int pid)
{
	int	__res;

	__asm__ volatile( "syscall"
			: "=a" (__res)
			: "a" (__NR_get_pid_info),
			  "b" (ret),
			  "c" (pid)
			: "memory");
	if (__res < 0) {
		errno = -__res;
		__res = -1;
	}
	return __res;
}

#endif
